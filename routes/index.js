var express = require('express');
var router = express.Router();
var config = require('../config/config.js')


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {title: 'scheduler'})
});

router.get('/views/:page', function(req, res, next) {
	res.render('partials/' + req.params.page)
})

router.get('/views/:folder/:page', function(req, res, next) {
	res.render('partials/' + req.params.folder + '/' + req.params.page)
})

router.get('*', function(req, res) {
	res.render('index', { title: 'scheduler' })
})

module.exports = router;
