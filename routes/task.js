var express = require('express')
var router = express.Router()
var User = require('../models/user')
var Task = require('../models/task')


router.route('/')
	.get(function(req, res) {
		var query = Task.find()
		
		query.populate('assigned', 'name _id')
		
		query.exec(function(err, tasks) {
			if(err) return res.send(err)
			
			return res.json({ success: true, tasks: tasks })
		})
	})
	
	.post(function(req, res) {
		var task = new Task(req.body)
		
		task.date = new Date()
		
		task.save(function(err) {
			if(err) return res.send(err)
			
			return res.json({ success: true, message: "Added task"})
		})
	})
	
	.delete(function(req, res) { 
		Task.remove(function(err) {
			if (err) return res.send(err)
			
			res.json({ success: true, message: 'All tasks deleted' })
		})
	})
	
router.route('/:task_id')
	.get(function(req, res) {
		var query = Task.findById(req.params.task_id)
		
		query.populate('assigned', 'name _id')
		
		query.exec(function(err, task) {
			if(err) return res.send(err)
			
			if(!task) return res.status(404).json({ success: false, message: "Task was not found"})
			
			return res.json({ success: true, task: task })
		})
	})
	
	.put(function(req, res) { 
		Task.findById(req.params.task_id, function(err, task) {
			if(err) return res.send(err)
			
			if(!task) return res.status(404).json({ success: false, message: "Task was not found" })
			
			for (var prop in req.body) {
				task[prop] = req.body[prop]
			}
			
			if(task.assigned.length > task.maxAssigned) {
				return res.json({ success: false, message: "That task is full" })
			}
			
			task.save(function(err) {
				if (err) return res.send(err)
				
				res.json({ success: true, message: "Task updated" })
			})
			
		})
	})
	
	.delete(function(req, res) {
		Task.findByIdAndRemove(req.params.task_id, function(err, task) {
			if(err) return res.send(err)
			
			if(!task) return res.status(404).json({ success: false, message: "Task was not Found" })
			
			return res.json({ success: true, message: "Task Deleted" })
		})
	})
	
router.route('/:task_id/assign/:user_id')

	.get(function(req, res) {		
		if(req.params.user_id === 'me'){
			var query = Task.findById(req.params.task_id)
			query.exec(function(err, task) {
				if(err) return res.send(err)
				
				if(!task) return res.status(404).json({ success: false, message: "Task was not found"})
				
				if(task.assigned.indexOf(req.user._id) > -1) {
					return res.json({ sucess: false, message: "That user is already assigned" })
				}
				else {
					task.assigned.push(req.user._id)
					
					task.save(function(err) {
						if (err) return res.send(err)
						
						return res.json({ success: true, message: "Task updated"})
					})
				}
			})
		}
		else {
			var userQuery = User.findById(req.params.user_id)
			
			userQuery.exec(function(err, user) {
				if(err) return res.status(404).json({ success: false, message: "Invalid User" , error: err})
				
				if(!user) return res.status(404).json({ success: false, message: "Could not find that user" })
				
				var taskQuery = Task.findById(req.params.task_id)
				
				taskQuery.exec(function(err, task) {
					if(err) return res.status(404).json({ success: false, message: "Invaild Task ID", error: err })
					
					if(!task) return res.status(404).json({ success: false, message: "Could not find that task" })
					
					if(task.assigned.indexOf(user._id) > -1) {
						return res.json({ sucess: false, message: "That user is already assigned" })
					}
					else {
						task.assigned.push(user._id)
					
						task.save(function(err) {
							if (err) return res.send(err)
						
						return res.json({ success: true, message: "Task updated"})
						})
					}
				})	
			})
		}
	})
	
	
router.route('/:task_id/unassign/:user_id')

	.get(function(req, res) {
		var query = Task.findById(req.params.task_id)
		query.exec(function(err, task) {
			if(err) return res.send(err)
			
			if(!task) return res.status(404).json({ success: false, message: "Task was not found"})
			
			var index = task.assigned.indexOf(req.params.user_id)
			
			if(req.params.user_id === 'me') {
				index = task.assigned.indexOf(req.user._id)
				console.log('me')
			}
			
			if(index < 0) {
				return res.status(404).json({ sucess: false, message: "That user is not assigned" })
			}
			else {
				task.assigned.splice(index, 1)
				
				task.save(function(err) {
					if (err) return res.send(err)
					
					res.json({ success: true, message: "Task updated"})
				})
			}
		})
	})
	
module.exports = router