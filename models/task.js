var mongoose = require('mongoose')
var Schema = mongoose.Schema

require('./user')

var TaskSchema = new Schema({ 
	taskName	:	{ type : String, required: true },
	description	:	{ type : String, required: true },
	date		:	{ type : Date, required: true },
	startTime	:	{ type : Date},
	endTime		:	{ type : Date},
	maxAssigned	:	{ type: Number, default: 10 },
	assigned	:	[{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]	
})


module.exports = mongoose.model('Task', TaskSchema)