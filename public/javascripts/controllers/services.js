angular.module('authService', [])

	.factory('Auth', function(AuthToken) {
		var authFactory = {}
		
		authFactory.login = function (username, password) {
			if(username === 'mhood' && password === 'password'){
				AuthToken.setLogin()
				return true
			}
			else {
				return false
			}
		}
		
		authFactory.logout = function() {
			AuthToken.setLogout()
		}
		
		authFactory.isLoggedIn = function () {
			if (AuthToken.getLogin()) {
				return true
			}
			else{
				return false
			}
		}
		
		return authFactory
	})
	
	.factory('AuthToken', function($window) {
		var authTokenFactory = {}
		
		authTokenFactory.setLogin = function() {
			$window.localStorage.setItem('loggedIn', 'true')
		}
		
		authTokenFactory.setLogout = function() {
			$window.localStorage.removeItem('loggedIn')
		}
		
		authTokenFactory.getLogin = function(logged) {
			return $window.localStorage.getItem('loggedIn')
		}
		
		
		return authTokenFactory
	})