var mainCtrl = angular.module('mainCtrl', [])

mainCtrl.controller('mainCtrl', function($rootScope, $state, Auth) {
		var self = this
		
		self.isLoggedIn = Auth.isLoggedIn()
		
		self.logout = function(){
			Auth.logout()
			$state.go($state.current, null, {reload: true})
		}
		
		$rootScope.$on('$stateChangeStart', function() {
			self.isLoggedIn = Auth.isLoggedIn()
		})
		
	}
)

var otherCtrl = angular.module('oCtrl', [])

otherCtrl.controller('oCtrl', function() {
	var self = this
	
	self.test = "ASDFASDFASDFASFDAS"
})

var loginCtrl = angular.module('loginForm', [])

loginCtrl.controller('loginFormCtrl', function($state, Auth) {
		var self = this
		
		if(Auth.isLoggedIn()){
			$state.transitionTo('home')
		}
		self.tester = function() {
			if(Auth.login(self.username, self.password)) {
				$state.go('home')
			}
		}
	}
)
