var appRoutes = angular.module('app.routes', ['ui.router', 'mainCtrl', 'loginForm'])

appRoutes.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
	$urlRouterProvider.otherwise('/home')
	
	$locationProvider.html5Mode(true)
	
	$stateProvider
	
		.state('home', {
			url: '/home',
			templateUrl: '/views/home'
		})
		
		.state('home.list', {
			url: '/list',
			templateUrl: 'views/home-list',
			controller: function($scope) {
				$scope.dogs = ['Bernese', 'Husky', 'Goldendoodle'];
			}
		})
		
		.state('home.paragraph', {
        	url: '/paragraph',
        	template: '<p> </p>'
    	})
		
		.state('about', {
			url: '/about',
			views: {
				'': { templateUrl: 'views/about' },
				'columnOne@about': { template: 'Look I am a column!' },
				'columnTwo@about': { 
					templateUrl: 'views/data',
					controller: 'testController'
				}
			}
		})
		
		.state('login', {
			url: '/login',
			templateUrl: '/views/login/login',
			controller: function($state){
				$state.transitionTo('login.form')
			}
		})
		
		.state('login.form', {
			templateUrl: 'views/login/login-form',
			controller: 'loginFormCtrl',
			controllerAs: 'login'
		})
		
		.state('login.register', {
			templateUrl: 'views/login/login-register'
		})
})

appRoutes.controller('testController', function($scope) {
	$scope.message = 'test'
	
	$scope.stuff = [
		{
			name: 'Macallan12',
			price: 50
		},
		{
			name: 'Chivas Regal Royal Salute',
			price: 10000
		},
		{
			name: 'Glenfiddich 1937',
			price: 20000
		}
	]
})